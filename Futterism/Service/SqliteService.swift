//
//  SqliteService.swift
//  YogaAppleTV
//
//  Created by Huydeptrai on 9/4/20.
//  Copyright © 2020 Huydeptrai. All rights reserved.
//

import UIKit
import SQLite

class SqliteService:NSObject {
    static let shared: SqliteService = SqliteService()
    var DatabaseRoot:Connection?
  
    var listDataKamus:[Tbl_kamusModel] = [Tbl_kamusModel]()
    
    func loadInit(linkPath:String){
        let urlVideo = Bundle.main.bundlePath
        do {
            self.DatabaseRoot = try Connection (linkPath)
        } catch {
            print(error)
        }   
    }
    
    func getDataKamus(closure: @escaping (_ response: [Tbl_kamusModel]?, _ error: Error?) -> Void) {
        let users1 = Table("topics")
        let topicId = Expression<Int>("topicId")
        let section = Expression<String>("section")
        let topicName = Expression<String>("topicName")
        let orderNumber = Expression<Int>("orderNumber")
        let fav = Expression<Int>("fav")
        listDataKamus.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    listDataKamus.append(Tbl_kamusModel( topicId: Int(user[topicId]) ,
                                                         section: String(user[section]) ,
                                                         topicName: String(user[topicName]) ,
                                                         orderNumber: Int(user[orderNumber]) ,
                                                         fav: Int(user[fav])
                    ))
                }
            } catch  {
            }
        }
        closure(listDataKamus, nil)
        
    }
    
    func updateFavourite(topic_send :Tbl_kamusModel,truefalse:Int  ,closure: @escaping (_ response: [Tbl_kamusModel]?, _ error: Error?) -> Void) {
        let users1 = Table("topics")
        let topicId = Expression<Int>("topicId")
        let section = Expression<String>("section")
        let topicName = Expression<String>("topicName")
        let orderNumber = Expression<Int>("orderNumber")
        let fav = Expression<Int>("fav")

        if let DatabaseRoot = DatabaseRoot{
            do{
                let alice = users1.filter(topicId == topic_send.topicId)
                try DatabaseRoot.run(alice.update(fav <- truefalse))
            }
            catch{

            }
        closure(listDataKamus, nil)

        }
    }
    
}
