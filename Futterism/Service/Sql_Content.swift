//
//  Sql_Content.swift
//  Futterism
//
//  Created by Apple on 09/07/2021.
//

import Foundation

//
//  SqliteService.swift
//  YogaAppleTV
//
//  Created by Huydeptrai on 9/4/20.
//  Copyright © 2020 Huydeptrai. All rights reserved.
//

import UIKit
import SQLite


class Sql_Contnent:NSObject {
    static let shared: Sql_Contnent = Sql_Contnent()
    var DatabaseRoot:Connection?
    var ListData:[Contents_Model] = [Contents_Model]()
    func loadInit(linkPath:String){
        let urlVideo = Bundle.main.bundlePath
        do {
            self.DatabaseRoot = try Connection (linkPath)
        } catch {
            print(error)
        }
    }
    
    func getData(closure: @escaping (_ response: [Contents_Model]?, _ error: Error?) -> Void) {
        let users1 = Table("subTopics")
        let subTopicId = Expression<Int>("subTopicId")
        let title = Expression<String>("title")
        let content = Expression<String>("content")
        let orderNumber = Expression<Int>("orderNumber")
        let topicKey = Expression<Int>("topicKey")
        let favourite = Expression<Int>("favourite")
        ListData.removeAll()
        if let DatabaseRoot = DatabaseRoot{
            do{
                for user in try DatabaseRoot.prepare(users1) {
                    ListData.append(Contents_Model( subTopicId: Int(user[subTopicId]) ,
                                                    title: String(user[title]) ,
                                                    content: String(user[content]) ,
                                                    orderNumber: Int(user[orderNumber]),
                                                    Topickey : Int(user[topicKey]),
                                                    favourite: Int(user[favourite])
                    
                    ))
                }
            } catch  {
            }
        }
        closure(ListData, nil)
        
    }
   
    
}
