//
//  Conten_Model.swift
//  Futterism
//
//  Created by Apple on 09/07/2021.
//


import Foundation
import SQLite3
import RealmSwift
//CREATE TABLE `tbl_kamus` (
//    `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
//    `kata`    TEXT,
//    `arti`    TEXT
//)

class Contents_Model 
{
    var subTopicId: Int = 0
     var title: String = ""
    var content: String = ""
     var orderNumber: Int = 0
    var Topickey: Int = 0
    var favourite: Int = 0
    
    init(subTopicId:Int, title:String, content: String,orderNumber: Int,Topickey :Int,favourite : Int){
    
        self.subTopicId = subTopicId
        self.title = title
        self.content = content
        self.orderNumber = orderNumber
        self.Topickey = Topickey
        self.favourite = favourite
    }
}
