//
//  dataModel.swift
//  TuDien
//
//  Created by devsenior1 on 10/05/2021.
//

import Foundation
import SQLite3
//CREATE TABLE `tbl_kamus` (
//    `id`    INTEGER PRIMARY KEY AUTOINCREMENT,
//    `kata`    TEXT,
//    `arti`    TEXT
//)

class Tbl_kamusModel
{
    var topicId: Int = 0
    var section: String = ""
    var topicName: String = ""
    var orderNumber: Int = 0
    var fav: Int = 0

    init(topicId:Int, section:String, topicName: String, orderNumber: Int, fav :Int){
        self.topicId = topicId
        self.section = section
        self.topicName = topicName
        self.orderNumber = orderNumber
        self.fav = fav
    }
    
}
