//
//  ContentInCollectionViewCell.swift
//  Futterism
//
//  Created by Apple on 08/07/2021.
//

import UIKit

class ContentInCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var textView: UITextView!
   
    @IBOutlet weak var book: UIButton!
    @IBOutlet weak var LabelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
