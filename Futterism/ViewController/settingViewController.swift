//
//  settingViewController.swift
//  Futterism
//
//  Created by Apple on 13/07/2021.
//

import UIKit
import StoreKit
import MessageUI


class settingViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{
    var ArrayOption:[String]=["Favorite","Rate","Share App","Change Color","Email Support",
                              "Report A bug","Feature Request"]
    var ArrayImage:[String] = ["1","2","3","4","5","5","5"]
    @IBOutlet weak var Futterrismtext: UILabel!
    var backgroundsetting :UIImageView!
    @IBOutlet var viewtong: UIView!
    @IBOutlet weak var SettingCollection: UICollectionView!
    var background :UIImageView!
    @IBOutlet weak var btnback: UIButton!
    @IBOutlet weak var viewFU_Co: UIView!
    var mausetting:UIColor!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        SettingCollection.layer.cornerRadius = 20
        
        
        let nib = UINib(nibName: "SettingCollectionViewCell", bundle: nil)
        SettingCollection.register(nib, forCellWithReuseIdentifier: "SettingCollectionViewCell")
        SettingCollection.delegate = self
        SettingCollection.dataSource = self
        
        // Do any additional setup after loading the view.
        viewtong.backgroundColor = .clear
        
        viewFU_Co.backgroundColor = mausetting
        viewFU_Co.layer.cornerRadius = 20
            
        
        
        
//
//
        //@ngoai overise
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return ArrayOption.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SettingCollectionViewCell", for: indexPath as IndexPath) as! SettingCollectionViewCell
        
        cell.LabelOption.text = ArrayOption[indexPath.row]
        cell.LabelOption.textColor = HomeViewController.mauchu
        cell.image_option.image = UIImage(named: ArrayImage[indexPath.row])
            return cell
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true)
        mausetting = viewFU_Co.backgroundColor
        
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            print("1")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController
            vc.background = background
            
            vc.navigationController?.pushViewController(vc, animated: true)
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
            
        case 1:
            print("2")
            guard let sce = view.window?.windowScene else {
                print("dat dep trai")
                return
            }
            SKStoreReviewController.requestReview(in: sce)
            print("2")
        case 2:
            print("3")
            let text = "Share for every one"
            let URL:NSURL = NSURL(string: "https://www.google.co.in")!
            let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
            if let popoverControler = vc.popoverPresentationController{
                popoverControler.sourceView = self.view
                popoverControler.sourceRect = self.view.bounds
            }
            self.present(vc, animated: true,completion:  nil)
        case 3:
            print("4")
            viewFU_Co.isHidden = true
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ColorViewController") as! ColorViewController
            vc.navigationController?.pushViewController(vc, animated: true)
            vc.view1 = viewFU_Co
            vc.background = background
            vc.mau = mausetting
            vc.colordelegate = self
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
            
        case 4:
            print("1122")
                let emailTitle = "Feedback"
                let messageBody = "Feature request or bug report?"
                let toRecipents = ["friend@stackoverflow.com"]
                guard MFMailComposeViewController.canSendMail() else {
                        return
                    }
                let mc: MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setSubject(emailTitle)
                mc.setMessageBody(messageBody, isHTML: false)
                mc.setToRecipients(toRecipents)
            self.present(mc, animated: true)
            
        case 5:
            let recipientEmail = "test@email.com"
                        let subject = "Multi client email support"
                        let body = "This code supports sending email via multiple different email apps on iOS! :)"
                        
                        // Show default mail composer
                        if MFMailComposeViewController.canSendMail() {
                            let mail = MFMailComposeViewController()
                            mail.mailComposeDelegate = self
                            mail.setToRecipients([recipientEmail])
                            mail.setSubject(subject)
                            mail.setMessageBody(body, isHTML: false)
                            
                            present(mail, animated: true)
                        
                        // Show third party email composer if default Mail app is not present
                        } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                            UIApplication.shared.open(emailUrl)
                        }
                    
                    
                    

//
        case 6:
            print("1122")
                let emailTitle = "Feedback"
                let messageBody = "Feature request or bug report?"
                let toRecipents = ["friend@stackoverflow.com"]
                guard MFMailComposeViewController.canSendMail() else {
                        return
                    }
                let mc: MFMailComposeViewController = MFMailComposeViewController()
                mc.mailComposeDelegate = self
                mc.setSubject(emailTitle)
                mc.setMessageBody(messageBody, isHTML: false)
                mc.setToRecipients(toRecipents)

            self.present(mc, animated: true)
        default:
            print("")
        }
    }
    
}


extension settingViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width
        let yourHeight = collectionView.bounds.height/7
        return CGSize(width: yourWidth, height: yourHeight)
        
    }
    
    
}
extension settingViewController : MFMailComposeViewControllerDelegate{
    func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
                controller.dismiss(animated: true)
            }
}

extension settingViewController: ColorDelegate{
    func chuyenmau(mau: UIColor,mauchu: UIColor) {
        viewFU_Co.backgroundColor = mau
    }
}
