//
//  ContentsInViewController.swift
//  Futterism
//
//  Created by Apple on 08/07/2021.
//

import SQLite
import UIKit

class ContentsInViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {

    
    
    @IBOutlet weak var HomeView: UIImageView!
    @IBOutlet var viewtong: UIView!
    @IBOutlet weak var LabelTitle: UILabel!
    @IBOutlet weak var BtnShare: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
   
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var ViewTitle: UIView!
    @IBOutlet weak var ContentsInCollection: UICollectionView!
    
    var ListData:[Contents_Model] = [Contents_Model]()
    var data: String = "" // la title course khi an vao hien len tren dau man hinh content
    
    
    var topic_id:Int = 0 //tim topic id = topickey
    var topic_send : Tbl_kamusModel = Tbl_kamusModel(topicId: 0, section: "", topicName: "", orderNumber: 0, fav: 0)
    
    var bo_n: String = ""
    var bo_ngoac1: String = ""
    var bo_ngoac2: String = ""
    var processData:[Contents_Model] = [Contents_Model]()   //  mang chua cai theo nhu cau
    var AfterSort:[Contents_Model] = [Contents_Model]()    // mang sau sap xep mang processData
    var dataBoimau: Substring = ""
    var bigtext:Substring = ""
    var ArrayBlueText:[String] = []
    var ArrayBigText:[String] = []
    var listDataOffline:[Tbl_kamusModel] = [Tbl_kamusModel]()
    @IBOutlet weak var btnlike2: UIButton!
    var background: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ContentInCollectionViewCell", bundle: nil)
        ContentsInCollection.register(nib, forCellWithReuseIdentifier: "ContentInCollectionViewCell")
        ContentsInCollection.delegate = self
        ContentsInCollection.dataSource = self
        LabelTitle.text = data    // ten course 
//
//        
        Sql_Contnent.shared.getData(){ [self] repond,error in
         if let repond = repond {
             self.ListData = repond
             self.ContentsInCollection.reloadData()
         }
            
        SqliteService.shared.getDataKamus(){ [self] repond,error in
             if let repond = repond{
                 self.listDataOffline = repond
                self.ContentsInCollection.reloadData()
                }
             }
        HomeView.image = background.image
        // font label title
        if (UIDevice.current.userInterfaceIdiom == .pad){
            LabelTitle.font = UIFont.systemFont(ofSize: 40)
            LabelTitle.textColor = .systemPink
        }
        else{
            LabelTitle.font = UIFont.systemFont(ofSize: 25)
            LabelTitle.textColor = .systemPink
        }
        
        ViewTitle.backgroundColor = .clear
        
            
    }
        // favorite
        
//
      
        if (topic_send.fav == 1) {
                self.btnLike.isHidden = true
                self.btnlike2.isHidden = false
                }
            else {
                    self.btnLike.isHidden = false
                    self.btnlike2.isHidden = true
                }
       
        //favorite
        }
    
        
    //@ngoai overise
    @IBAction func lovePressed(sender: AnyObject) {
        self.btnLike.isHidden = false
        self.btnlike2.isHidden = true
        SqliteService.shared.updateFavourite(topic_send: topic_send,truefalse: 0 ){ _,_ in
            
        }
        print("123")
        
    }
    
    @IBAction func lovePressedRed(sender: AnyObject) {
        self.btnLike.isHidden = true
        self.btnlike2.isHidden = false
        SqliteService.shared.updateFavourite(topic_send: topic_send,truefalse: 1 ){ _,_ in
            
        }
        print("123")
    }
    
    @IBAction func ActionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
      
    }
    
    
    @IBAction func BtnShare(_ sender: Any) {
        let text = "Share for every one"
        let URL:NSURL = NSURL(string: "https://www.google.co.in")!
        let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
        if let popoverControler = vc.popoverPresentationController{
            popoverControler.sourceView = self.view
            popoverControler.sourceRect = self.view.bounds
        }
        self.present(vc, animated: true,completion:  nil)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // lay answer
        for item in ListData{
            if item.Topickey == topic_send.topicId{
                   processData.append(item)
                       }
                   }
        AfterSort = processData.sorted(by:{ $0.orderNumber < $1.orderNumber})
        return 1
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellContent = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentInCollectionViewCell", for: indexPath) as! ContentInCollectionViewCell
        
        // + sap xep procees theo ordernumber tang dan
        // + xu ly String text
        cellContent.LabelTitle.text = AfterSort[indexPath.row].title
        cellContent.layer.cornerRadius = 20
//        cellContent.textView.text = bo_ngoac2
        cellContent.textView.font = UIFont.systemFont(ofSize: 20)
        
        cellContent.backgroundColor = .clear
        
        let a = AfterSort[indexPath.row].content
        bo_ngoac1 = a.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")

        bo_n = a.replacingOccurrences(of: "\\n", with: "\n ")
        bo_ngoac1 = bo_n.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        bo_ngoac2 = bo_ngoac2.replacingOccurrences(of: "**", with: "*")
        
        if bo_ngoac2.count > 0{
            bo_ngoac2.removeFirst()
            bo_ngoac2.removeLast()
        //clean data
        }
        //clean data
        print (bo_ngoac2)
        //clean data text color to blue
        while (bo_ngoac2.firstIndex(of: "`") != nil){
               if let first = bo_ngoac2.firstIndex(of: "`") {
                   bo_ngoac2.remove(at: first)
                   bo_ngoac2.insert(" ", at: first)
                   let range = bo_ngoac2.index(first, offsetBy: 1)..<bo_ngoac2.endIndex
                   var array_string = bo_ngoac2[range]
                   if var end = array_string.firstIndex(of: "`") {
                       bo_ngoac2.remove(at: end)
                       bo_ngoac2.insert(" ", at: end)
                       dataBoimau = bo_ngoac2[first...end]
                       ArrayBlueText.append(String(dataBoimau))
                       
                   }
               }
           }
        cellContent.textView.textColor = HomeViewController.mauchu
        let attributedWithTextColor: NSAttributedString = bo_ngoac2.attributedStringWithColor(ArrayBlueText, color: UIColor.blue)
        
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        let gradient:CAGradientLayer = CAGradientLayer()
        
        gradient.colors = [UIColor(red: 116/255, green: 114/255, blue: 143/255, alpha: 1.0).cgColor,UIColor(red: 115/255, green: 114/255, blue: 142/255, alpha: 1/0).cgColor]
//        gradient.locations = [0.0,1.0]
        gradient.frame = view.bounds
        cellContent.view.layer.insertSublayer(gradient, at: 0)
//        cellContent.view.backgroundColor = .white
        cellContent.LabelTitle.textColor = .black
        //
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width) - 70, height: Int(UIScreen.main.bounds.height)))
        let colortextview:CAGradientLayer = CAGradientLayer()
        colortextview.colors = [UIColor(red: 115/255, green: 113/255, blue: 142/255, alpha: 1.0).cgColor,UIColor(red: 75/255, green: 74/255, blue: 105/255, alpha: 1/0).cgColor,UIColor(red: 49/255, green: 33/255, blue: 95/255, alpha: 1.0).cgColor]
//        colortextview.locations = [0.0,1.0]
        colortextview.frame = view.bounds
        cellContent.textView.layer.insertSublayer(colortextview, at: 0)
//        cellContent.textView.backgroundColor = UIColor(red: 109/255, green: 66/255, blue: 232/255, alpha: 0.9)
        //
        cellContent.textView.attributedText = attributedWithTextColor
        
        // clean data textcolor to blue
        //clean data to todam
      
        
        // clean data to todam
        return cellContent
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return processData.count
    }
    
    
    

}
extension ContentsInViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var countLines = 20   //dem so dong
        var heighPro = 1
        let a = AfterSort[indexPath.row].content
        // clean data
        bo_ngoac1 = a.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        bo_n = a.replacingOccurrences(of: "\\n", with: "\n ")
        bo_ngoac1 = bo_n.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        
        for c in bo_ngoac2{
            if c == "\n"{
                countLines += 3 //dem ca nhung dong trong
            }
            let lines = bo_ngoac2.split(whereSeparator: \.isNewline) //tach cac hang co ki tu luu vao mang
//            for item in lines{ //trong moi dong da tach
                let words = lines.count  //dem xem co bao nhieu tu
                let length = words  / Int(UIScreen.main.bounds.height - 40) //(trung binh moi tu rong 13pxl, tinh do dai ca cau) / do rong cua cell hien thi de biet phai ngat thanh bao nhieu dong
//                if length > 1 { //neu cau do dai, can xuong dong
            heighPro = heighPro + length * 9 //chieu cao = do cao 1 dong * so dong
//                }
//            }

        }
        heighPro += countLines * 6
        
        return CGSize(width: Int(UIScreen.main.bounds.width) - 70, height: heighPro)
        }
    
}

extension String {
    func attributedStringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }

        guard let characterSpacing = characterSpacing else {return attributedString}

        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

        return attributedString
    }
    
    
    
}

    

    




