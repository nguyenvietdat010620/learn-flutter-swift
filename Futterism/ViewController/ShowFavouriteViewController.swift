//
//  ShowFavouriteViewController.swift
//  Futterism
//
//  Created by Apple on 08/08/2021.
//

import UIKit

class ShowFavouriteViewController: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource{
    var datashowFavourite:[Tbl_kamusModel] = [Tbl_kamusModel]()
    var process:[Contents_Model] = [Contents_Model]()
    var AfterSort:[Contents_Model] = [Contents_Model]()
    var processData:[Tbl_kamusModel] = [Tbl_kamusModel]()
    @IBOutlet weak var ShowfavouriteCollection: UICollectionView!
    
    
    @IBOutlet weak var labeltitle: UILabel!
    @IBOutlet weak var HomeView: UIImageView!
    var topic_id: Int = 0
    var bo_n: String = ""
    var bo_ngoac1: String = ""
    var bo_ngoac2: String = ""
    var dataBoimau: Substring = ""
    var bigtext:Substring = ""
    var ArrayBlueText:[String] = []
    var ListData:[Contents_Model] = [Contents_Model]()
    var background :UIImageView!
    @IBOutlet weak var viewbtn: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "ShowFavouriteCollectionViewCell", bundle: nil)
        ShowfavouriteCollection.register(nib, forCellWithReuseIdentifier: "ShowFavouriteCollectionViewCell")
        ShowfavouriteCollection.delegate = self
        ShowfavouriteCollection.dataSource = self
        // Do any additional setup after loading the view.
        Sql_Contnent.shared.getData(){ [self] repond,error in
         if let repond = repond {
             self.ListData = repond
             self.ShowfavouriteCollection.reloadData()
         }
        }
        viewbtn.backgroundColor = .clear
        HomeView.image = background.image
        labeltitle.textColor = .white
        //ngoai view điload
    }
    
    @IBAction func btnback(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        for item in datashowFavourite{
            if (item.topicId == topic_id){
                processData.append(item)
            }
        }
            
            
        for item in processData{
            for item1 in ListData{
                if(item.topicId == item1.Topickey ){
                    process.append(item1)
                }
            }
        }
        
        AfterSort = process.sorted(by:{ $0.orderNumber < $1.orderNumber})
        
        print("AfterSort.count: \(AfterSort.count)")
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AfterSort.count
    }
    
    @IBAction func sharebtn(_ sender: Any) {
        let text = "Share for every one"
        let URL:NSURL = NSURL(string: "https://www.google.co.in")!
        let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
        if let popoverControler = vc.popoverPresentationController{
            popoverControler.sourceView = self.view
            popoverControler.sourceRect = self.view.bounds
        }
        self.present(vc, animated: true,completion:  nil)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowFavouriteCollectionViewCell", for: indexPath as IndexPath) as! ShowFavouriteCollectionViewCell
        
        let a = AfterSort[indexPath.row].content
        bo_ngoac1 = a.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")

        bo_n = a.replacingOccurrences(of: "\\n", with: "\n ")
        bo_ngoac1 = bo_n.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        bo_ngoac2 = bo_ngoac2.replacingOccurrences(of: "**", with: "*")
        
        if bo_ngoac2.count > 0{
        bo_ngoac2.removeFirst()
        bo_ngoac2.removeLast()
        //clean data
        }
        print (bo_ngoac2)
        //clean data text color to blue
        while (bo_ngoac2.firstIndex(of: "`") != nil){
               if let first = bo_ngoac2.firstIndex(of: "`") {
                   bo_ngoac2.remove(at: first)
                   bo_ngoac2.insert(" ", at: first)
                   let range = bo_ngoac2.index(first, offsetBy: 1)..<bo_ngoac2.endIndex
                   var array_string = bo_ngoac2[range]
                   if var end = array_string.firstIndex(of: "`") {
                       bo_ngoac2.remove(at: end)
                       bo_ngoac2.insert(" ", at: end)
                       dataBoimau = bo_ngoac2[first...end]
                       ArrayBlueText.append(String(dataBoimau))
                       
                   }
               }
           }
        let attributedWithTextColor: NSAttributedString = bo_ngoac2.StringWithColor(ArrayBlueText, color: UIColor(red: 13/255, green: 233/255, blue: 182/255, alpha: 1))
        //
//        let gradient:CAGradientLayer = CAGradientLayer()
//
//        gradient.colors = [UIColor(red: 116/255, green: 114/255, blue: 143/255, alpha: 1.0).cgColor,UIColor(red: 115/255, green: 114/255, blue: 142/255, alpha: 1/0).cgColor]
////        gradient.locations = [0.0,1.0]
//        gradient.frame = view.bounds
//        cell.viewtitle.layer.insertSublayer(gradient, at: 0)
        cell.viewtitle.backgroundColor = .white
        //
//        let colortextview:CAGradientLayer = CAGradientLayer()
//        colortextview.colors = [UIColor(red: 115/255, green: 113/255, blue: 142/255, alpha: 1.0).cgColor,UIColor(red: 75/255, green: 74/255, blue: 105/255, alpha: 1/0).cgColor,UIColor(red: 49/255, green: 33/255, blue: 95/255, alpha: 1.0)]
////        colortextview.locations = [0.0,1.0]
//        colortextview.frame = view.bounds
//        cell.showfavourite_textview.layer.insertSublayer(colortextview, at: 0)
//        cell.showfavourite_textview.backgroundColor = UIColor(red: 109/255, green: 66/255, blue: 232/255, alpha: 0.9)
        cell.showfavourite_textview.backgroundColor = .white
        //
        cell.label_title.textColor = .black
        cell.layer.cornerRadius = 20
        cell.label_title.text = AfterSort[indexPath.row].title
        cell.showfavourite_textview.textColor = .white
        cell.showfavourite_textview.attributedText = attributedWithTextColor
        
        return cell
    }
}
extension String {
    func StringWithColor(_ strings: [String], color: UIColor, characterSpacing: UInt? = nil) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self)
        for string in strings {
            let range = (self as NSString).range(of: string)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        }

        guard let characterSpacing = characterSpacing else {return attributedString}

        attributedString.addAttribute(NSAttributedString.Key.kern, value: characterSpacing, range: NSRange(location: 0, length: attributedString.length))

        return attributedString
    }
}

extension ShowFavouriteViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var countLines = 20   //dem so dong
        var heighPro = 1
        let a = AfterSort[indexPath.row].content
        // clean data
        bo_ngoac1 = a.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        bo_n = a.replacingOccurrences(of: "\\n", with: "\n ")
        bo_ngoac1 = bo_n.replacingOccurrences(of: "[", with: "")
        bo_ngoac2 = bo_ngoac1.replacingOccurrences(of: "]", with: "")
        
        for c in bo_ngoac2{
            if c == "\n"{
                countLines += 3 //dem ca nhung dong trong
            }
            let lines = bo_ngoac2.split(whereSeparator: \.isNewline) //tach cac hang co ki tu luu vao mang
//            for item in lines{ //trong moi dong da tach
                let words = lines.count  //dem xem co bao nhieu tu
                let length = words  / Int(UIScreen.main.bounds.height - 40) //(trung binh moi tu rong 13pxl, tinh do dai ca cau) / do rong cua cell hien thi de biet phai ngat thanh bao nhieu dong
//                if length > 1 { //neu cau do dai, can xuong dong
            heighPro = heighPro + length * 9 //chieu cao = do cao 1 dong * so dong
//                }
//            }

        }
        heighPro += countLines * 6
        return CGSize(width: Int(UIScreen.main.bounds.width) - 70, height: heighPro)
        }
    
}



