//
//  HomeViewController.swift
//  Futterism
//
//  Created by Apple on 03/07/2021.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate,  UICollectionViewDataSource{
    
    @IBOutlet weak var Homeview: UIImageView!
    var listimage:[String] = ["imagebeginer","Imageintermediate","ImageAdvanced"]
    
    var listCourse:[String] = ["Learn Flutter Beginner", "Learn Flutter Intermediate"
                               ,"Learn Flutter Advanced"]
//    var mau:UIColor!
    static var mau:UIColor = UIColor(red: 63/255, green: 73/255, blue: 159/255, alpha: 0.96)
    static var mauchu:UIColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1)
    @IBOutlet var ViewTong: UIView!
    
    @IBOutlet weak var labelHome: UILabel!
    
    @IBOutlet weak var HomeCollection: UICollectionView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let nib = UINib(nibName: "HomeCollectionViewCell", bundle: nil)
        HomeCollection.register(nib, forCellWithReuseIdentifier: "HomeCollectionViewCell")
        HomeCollection.delegate = self
        HomeCollection.dataSource = self
        
        ViewTong.backgroundColor = HomeViewController.mau
        labelHome.textColor = .white
    }
    
    @IBAction func Setting(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "settingViewController") as! settingViewController
        vc.background = Homeview
        vc.mausetting = HomeViewController.mau
        
        vc.navigationController?.pushViewController(vc, animated: true)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true)
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return listimage.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as! HomeCollectionViewCell
        
        
        
        
        cell.labelCourse.text = listCourse[indexPath.row]
        
        // size chu tuy phone
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            cell.labelCourse.font = UIFont.systemFont(ofSize: 40.0)
            
        }
        else{
            cell.labelCourse.font = UIFont.systemFont(ofSize: 20.0)
            
        }
        
        cell.imageView?.image = UIImage(named: listimage[indexPath.row])
        cell.imageView?.layer.cornerRadius = 20
        cell.labelCourse?.textColor = HomeViewController.mauchu
        cell.backgroundColor = .clear
        cell.setNeedsLayout()
        return cell
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            print("0")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "courseViewController") as! courseViewController
            vc.location = indexPath.row
            vc.background = Homeview
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
            
        case 1:
            print("1")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "courseViewController") as! courseViewController
            vc.location = indexPath.row
            vc.background = Homeview
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
        case 2:
            print("2")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "courseViewController") as! courseViewController
            vc.location = indexPath.row
            vc.background = Homeview
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
        default:
            print("3")
        }
    }
    
}

extension HomeViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let yourWidth = view.frame.width/1.3
            let yourHeight = collectionView.bounds.height/3
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else{
            let yourWidth = view.frame.width/1.2
            let yourHeight = collectionView.bounds.height/2.4
            return CGSize(width: yourWidth, height: yourHeight)
    }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(20)
    }
    
    
}







