//
//  FavouriteViewController.swift
//  Futterism
//
//  Created by Apple on 19/07/2021.
//

import UIKit

class FavouriteViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate{
    
    @IBOutlet weak var favourite: UILabel!
    var filterdata:[Tbl_kamusModel] = [Tbl_kamusModel]()
    var listDataOffline:[Tbl_kamusModel] = [Tbl_kamusModel]()
    var trueFavourite:[Tbl_kamusModel] = [Tbl_kamusModel]()
    @IBOutlet weak var FavouriteCollection: UICollectionView!
    var background: UIImageView!
    @IBOutlet weak var HomeView: UIImageView!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var viewSearchbar: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SqliteService.shared.getDataKamus(){ [self] repond,error in
         if let repond = repond{
             self.listDataOffline = repond
             self.FavouriteCollection.reloadData()
            }
        }
        //
        for item in listDataOffline{
            if item.fav == 1{
                trueFavourite.append(item)
                print(trueFavourite.count)
            }
        }
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "FavouriteCollectionViewCell", bundle: nil)
        FavouriteCollection.register(nib, forCellWithReuseIdentifier: "FavouriteCollectionViewCell")
        FavouriteCollection.delegate = self
        FavouriteCollection.dataSource = self
        filterdata = trueFavourite
        viewSearchbar.backgroundColor = .clear
        searchbar.backgroundColor = .clear
        HomeView.image = background.image
        favourite.textColor = .white
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterdata.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteCollectionViewCell", for: indexPath as IndexPath) as! FavouriteCollectionViewCell
        cell.LabelCourse.text = filterdata[indexPath.row].topicName
        cell.LabelCourse.textColor = .white
        cell.LabelCourse.font = UIFont.systemFont(ofSize: 30.0)
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterdata = []
            if searchText == ""{
                filterdata = trueFavourite
            }
            else{
            for item in trueFavourite{
                if item.topicName.lowercased().contains(searchText.lowercased()){
                    filterdata.append(item)
                }
            }
            }
            self.FavouriteCollection.reloadData()
        }
    
    
    
    @IBAction func backbtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sharebtn(_ sender: Any) {
        let text = "Share for every one"
        let URL:NSURL = NSURL(string: "https://www.google.co.in")!
        let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
        if let popoverControler = vc.popoverPresentationController{
            popoverControler.sourceView = self.view
            popoverControler.sourceRect = self.view.bounds
        }
        self.present(vc, animated: true,completion:  nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0...indexPath.row{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ShowFavouriteViewController") as! ShowFavouriteViewController
            
            vc.datashowFavourite = filterdata
            vc.topic_id = filterdata[i].topicId
            vc.background = HomeView
            vc.navigationController?.pushViewController(vc, animated: true)
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true)
        }
    }
    
    
}

extension FavouriteViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let yourWidth = view.frame.width
            let yourHeight = collectionView.bounds.height/20
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else{
            let yourWidth = collectionView.bounds.width
            let yourHeight = collectionView.bounds.height/15
            return CGSize(width: yourWidth, height: yourHeight)
    }
}
    
}

