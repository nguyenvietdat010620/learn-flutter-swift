//
//  ColorViewController.swift
//  Futterism
//
//  Created by Apple on 16/07/2021.
//

import UIKit
protocol ColorDelegate {
    func chuyenmau(mau: UIColor,mauchu : UIColor)
}
class ColorViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    var view1: UIView!
    @IBOutlet var viewTong: UIView!
    @IBOutlet weak var ColorCollection: UICollectionView!
    @IBOutlet weak var viewColor: UIView!
    var colordelegate: ColorDelegate!
    var mau: UIColor!
    var ktra: Int!
    var background: UIImageView!
    
    var mauchu: UIColor!
    
    var ArrayColor:[String] = ["Dark","Grey","Blue","Blue Dark","Amber","Blue Grey","Brow Dark","Brow","Cyan","Cyan Dark","Deep Orange"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "ColorCollectionViewCell", bundle: nil)
        ColorCollection.register(nib, forCellWithReuseIdentifier: "ColorCollectionViewCell")
        ColorCollection.delegate = self
        ColorCollection.dataSource = self
        //
        
       
        viewColor.backgroundColor = mau
        //
        viewColor.layer.cornerRadius = 20
        viewTong.backgroundColor = .clear
        ColorCollection.backgroundColor = .clear
        
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArrayColor.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCollectionViewCell", for: indexPath as IndexPath) as! ColorCollectionViewCell
        cell.Color.text = ArrayColor[indexPath.row]
        cell.Color.textColor = HomeViewController.mauchu
        
        return cell
    }
    
    
    @IBAction func btnbacktong(_ sender: Any){
        dismiss(animated: true)
        view1.isHidden = false
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
       
            
        case 0:
            background.image = UIImage(named: "xanhden")
            colordelegate.chuyenmau(mau: UIColor(red: 11/255, green: 27/255, blue: 48/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor = UIColor(red: 11/255, green: 27/255, blue: 48/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 11/255, green: 27/255, blue: 48/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 1:
            background.image = UIImage(named: "grey")
            colordelegate.chuyenmau(mau: UIColor(red: 175/255, green: 181/255, blue: 181/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            HomeViewController.mau = UIColor(red: 175/255, green: 181/255, blue: 181/255, alpha: 0.96)
            viewColor.backgroundColor = UIColor(red: 175/255, green: 181/255, blue: 181/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 2:
            background.image = UIImage(named: "xanhnuocbien")
            colordelegate.chuyenmau(mau: UIColor(red: 14/255, green: 92/255, blue: 244/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            HomeViewController.mau = UIColor(red: 14/255, green: 92/255, blue: 244/255, alpha: 0.96)
            viewColor.backgroundColor = UIColor(red: 14/255, green: 92/255, blue: 244/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 3:
            background.image = UIImage(named: "bluedark")
            colordelegate.chuyenmau(mau: UIColor(red: 18/255, green: 41/255, blue: 102/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor = UIColor(red: 18/255, green: 41/255, blue: 102/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 18/255, green: 41/255, blue: 102/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
            
        case 4:
            background.image = UIImage(named: "amber")
            colordelegate.chuyenmau(mau: UIColor(red: 225/255, green: 219/255, blue: 77/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            
            viewColor.backgroundColor = UIColor(red: 225/255, green: 219/255, blue: 77/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 225/255, green: 219/255, blue: 77/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 5:
            background.image = UIImage(named: "bluegray")
            colordelegate.chuyenmau(mau: UIColor(red: 151/255, green: 187/255, blue: 194/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor = UIColor(red: 151/255, green: 187/255, blue: 194/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 225/255, green: 219/255, blue: 77/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 6:
            background.image = UIImage(named: "browdark")
            colordelegate.chuyenmau(mau: UIColor(red: 153/255, green: 100/255, blue: 21/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor = UIColor(red: 153/255, green: 100/255, blue: 21/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 153/255, green: 100/255, blue: 21/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 7:
            background.image = UIImage(named: "brow")
            colordelegate.chuyenmau(mau: UIColor(red: 217/255, green: 134/255, blue: 36/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor = UIColor(red: 217/255, green: 134/255, blue: 36/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 217/255, green: 134/255, blue: 36/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 14/255, green: 92/255, blue: 244/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 8:
            background.image = UIImage(named: "cyan")
            colordelegate.chuyenmau(mau: UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor =  UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 9:
            background.image = UIImage(named: "cyandark")
            colordelegate.chuyenmau(mau: UIColor(red: 20/255, green: 150/255, blue: 150/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor =  UIColor(red: 20/255, green: 150/255, blue: 150/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 20/255, green: 150/255, blue: 150/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 10:
            background.image = UIImage(named: "deeporange")
            colordelegate.chuyenmau(mau: UIColor(red: 255/255, green: 167/255, blue: 91/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor =  UIColor(red: 255/255, green: 167/255, blue: 91/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 255/255, green: 167/255, blue: 91/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 14/255, green: 92/255, blue: 244/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        case 11:
            background.image = UIImage(named: "green")
            colordelegate.chuyenmau(mau: UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 0.96),mauchu: UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            viewColor.backgroundColor =  UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 0.96)
            HomeViewController.mau = UIColor(red: 0/255, green: 255/255, blue: 0/255, alpha: 0.96)
            HomeViewController.mauchu = UIColor(red: 225/255, green: 219/255, blue: 77/255, alpha: 1)
            dismiss(animated: true, completion: nil)
            view1.isHidden = false
        default:
            print("")
        }
    }


}

extension ColorViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        return CGFloat(30)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let yourWidth = view.frame.width
            let yourHeight = collectionView.bounds.height/20
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else{
            let yourWidth = collectionView.bounds.width
            let yourHeight = collectionView.bounds.height/15
            return CGSize(width: yourWidth, height: yourHeight)
    }
}
    
}


