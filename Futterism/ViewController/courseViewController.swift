//
//  courseViewController.swift
//  Futterism
//
//  Created by Apple on 07/07/2021.
//

import UIKit

class courseViewController: UIViewController ,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var location:Int = 0  // vi tri nut nhan tren man hinh home
    var Title_Course_In_ContentsCourse = ""
    
    @IBOutlet weak var ViewTop: UIView!
    var newArrayBegin :[Tbl_kamusModel] = [Tbl_kamusModel]()   // mang chua data cua course Begin
    var newArrayInta : [Tbl_kamusModel] = [Tbl_kamusModel]()   // mang chua data cua course Inta
    var newArrayAdvan : [Tbl_kamusModel] = [Tbl_kamusModel]()   // mang chua data cua course Advan
    
    var newArrayBegin1 :[Tbl_kamusModel] = [Tbl_kamusModel]()
    var newArrayInta1 : [Tbl_kamusModel] = [Tbl_kamusModel]()
    var newArrayAdvan1 : [Tbl_kamusModel] = [Tbl_kamusModel]()
    var listDataOffline:[Tbl_kamusModel] = [Tbl_kamusModel]()   // mang chua data cua 3 course
    var background: UIImageView!
    @IBOutlet weak var TitleCourse: UILabel!
    @IBOutlet weak var courseCollection: UICollectionView!
    // back man hinh home
    
    @IBAction func ActionBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var HomeView: UIImageView!
    @IBOutlet weak var BtnShare: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var Bi_In_Ad: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CourseCollectionViewCell", bundle: nil)
        courseCollection.register(nib, forCellWithReuseIdentifier: "CourseCollectionViewCell")
        
        // xu ly sql , load sql to list
        SqliteService.shared.getDataKamus(){ [self] repond,error in
         if let repond = repond{
             self.listDataOffline = repond
             self.courseCollection.reloadData()
         }
            
        }
        courseCollection.delegate = self
        courseCollection.dataSource = self
//       
        ViewTop.backgroundColor = UIColor(red: 9/255, green: 21/255, blue: 131/255, alpha: 0.8)
        
        
        HomeView.image = background.image
    }
    
    @IBAction func btnShare(_ sender: Any) {
        let text = "Share for every one"
        let URL:NSURL = NSURL(string: "https://www.google.co.in")!
        let vc = UIActivityViewController(activityItems: [text,URL], applicationActivities: [])
        if let popoverControler = vc.popoverPresentationController{
            popoverControler.sourceView = self.view
            popoverControler.sourceRect = self.view.bounds
        }
        self.present(vc, animated: true,completion:  nil)

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        for item in listDataOffline {
            if item.section == "Beginner"{
                newArrayBegin.append(item)
            }
//            newArrayBegin1 = newArrayBegin.sorted(by:{ $0.orderNumber < $1.orderNumber})
        }
        //
        for item in listDataOffline {
            if item.section == "Intermediate"{
                newArrayInta.append(item)
            }
            newArrayInta1 = newArrayInta.sorted(by:{ $0.orderNumber < $1.orderNumber})
        }
        for item in listDataOffline {
            if item.section == "Advanced"{
                newArrayAdvan.append(item)
            }
            newArrayAdvan1 = newArrayAdvan.sorted(by:{ $0.orderNumber < $1.orderNumber})
        }
        return 1
    }
    
    // so cell
    private func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) ->  Int {
        
        return indexPath.row
    }
    // so hang
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if location == 0 {
            return 30
        }
        if location == 1 {
            return 18
        }
        else {return 7}
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellcourse = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseCollectionViewCell", for: indexPath) as! CourseCollectionViewCell
    //load data len label
        switch location {
        case 0:
//            newArrayBegin = newArrayBegin.sorted(by:{ $0.ordernumber < $1.ordernumber})
            cellcourse.labelAnswer.text = newArrayBegin1[indexPath.row].topicName
            Bi_In_Ad.text = "Beginner"
            if (UIDevice.current.userInterfaceIdiom == .pad){
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 60)
                ViewTop.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 30)
            }
            else{
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 40)
                ViewTop.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 30)
            }
        case 1:
          print("")
//            newArrayInta = newArrayInta.sorted(by:{ $0.ordernumber < $1.ordernumber})
//            print("location"+String(location))
            cellcourse.labelAnswer.text = newArrayInta1[indexPath.row].topicName
            Bi_In_Ad.text = "Intermediate"
            if (UIDevice.current.userInterfaceIdiom == .pad){
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 60)
            }
            else{
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 40.0)
            }
            
        case 2:
            print("")
//            newArrayAdvan = newArrayAdvan.sorted(by:{ $0.ordernumber < $1.ordernumber})
            cellcourse.labelAnswer.text = newArrayAdvan1[indexPath.row].topicName
            Bi_In_Ad.text = "Advanced"
            if (UIDevice.current.userInterfaceIdiom == .pad){
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 60)
            }
            else{
                Bi_In_Ad.font = UIFont.systemFont(ofSize: 30)
            }
        default:
            print("23123")
        }
  // load data len label
        
        cellcourse.labelAnswer.textColor = HomeViewController.mauchu
        Bi_In_Ad.textColor = .red
        // chinh  size chu
        if (UIDevice.current.userInterfaceIdiom == .pad){
            cellcourse.labelAnswer.font = UIFont.systemFont(ofSize: 40.0)
        }
        else{
            cellcourse.labelAnswer.font = UIFont.systemFont(ofSize: 20.0)
        }
        cellcourse.backgroundColor = .clear
        return cellcourse
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        collectionView.backgroundColor = .clear
        if (UIDevice.current.userInterfaceIdiom == .pad){
            return CGFloat(70)
        }
        else{
            return CGFloat(45)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
        if (UIDevice.current.userInterfaceIdiom == .pad){
            let yourWidth = view.frame.width
            let yourHeight = collectionView.bounds.height/20
            return CGSize(width: yourWidth, height: yourHeight)
        }
        else{
            let yourWidth = view.frame.width
            let yourHeight = collectionView.bounds.height/15
            return CGSize(width: yourWidth, height: yourHeight)
    }
}
    
    //  load data len label
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0...indexPath.row{
            switch i {
            case i:
                if location == 0{
                
                            Title_Course_In_ContentsCourse = newArrayBegin1[indexPath.row].topicName
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ContentsInViewController") as! ContentsInViewController
                            vc.data = Title_Course_In_ContentsCourse
                            vc.background = HomeView
//                            vc.topic_id = newArrayBegin[indexPath.row].topicId
                            vc.topic_send = newArrayBegin1[indexPath.row]
                            vc.navigationController?.pushViewController(vc, animated: true)
                            vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: true)
                }
                if location == 1{
                                Title_Course_In_ContentsCourse = newArrayInta1[indexPath.row].topicName
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ContentsInViewController") as! ContentsInViewController
                                vc.data = Title_Course_In_ContentsCourse
                                vc.background = HomeView
                                vc.topic_send = newArrayInta1[indexPath.row]
                                vc.navigationController?.pushViewController(vc, animated: true)
                                vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: true)
                }
                if location == 2{
                                Title_Course_In_ContentsCourse = newArrayAdvan1[indexPath.row].topicName
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ContentsInViewController") as! ContentsInViewController
                                vc.data = Title_Course_In_ContentsCourse
                                vc.background = HomeView
//                                vc.topic_id = newArrayAdvan[indexPath.row].topicId
                                vc.topic_send = newArrayAdvan1[indexPath.row]
                                vc.navigationController?.pushViewController(vc, animated: true)
                                vc.modalPresentationStyle = .overFullScreen
                        self.present(vc, animated: true)
                }
                
            default:
                print("")
            }
            
        }

    }
    
    }


extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

